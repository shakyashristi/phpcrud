<?php include 'header.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
<style>
.col-sm-12{
    padding-top:40px;
    padding-left: 150px;
    
}
td,th {
  border: 1px solid #ddd;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2;}

tr:hover {background-color: #ddd;}
table{
    padding-top: 10px;
  padding-bottom: 10px;
  text-align: left;
  color: black;
}
}
.button{
    padding-left:200px;
    border-radius: 4px;
}
</style>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>
</body>
</html>

<div class="col-sm-12">
<?php
$servername='localhost';
$username='shristi';
$password='Shristi123!@#';
$dbname = "schooldb";


$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT std_id, name, address, DateofBirth, department_id, course_id FROM student";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>student_id</th>
    <th>Name</th>
    <th>Address</th>
    <th>DateofBirth</th>
    <th>Department_id</th>
    <th>Course_id</th>
    <th>Action</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["std_id"]."</td>";
        echo "<td>". $row["name"]."</td>";
        echo "<td>". $row["address"]."</td>";
        echo "<td>". $row["DateofBirth"]."</td>";
        echo "<td>". $row["department_id"]."</td>";
        echo "<td>". $row["course_id"]."</td>";
        echo '<td><a href="edit.php?id=' . $row['std_id'] . '">Edit</a> <a href="delete.php?id=' . $row['std_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>
</div>
<div class="button">
<a href="student.php" button type="button" class="btn btn-primary">Back</button>
</div>