<?php include 'index.php';?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href= "css/style.css" rel ="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<title>School Database Management System</title>
<style>
    .sidebar{
        padding-top:0px;
    }
    </style>
</head>
<body>



<div class="sidebar">
  <a class="active" href="student.php">Student</a>
  <a href="teacher.php">Teacher</a>
  <a href="department.php">Department</a>
  <a href="course.php">Course</a>
</div>

<section id="home">
<div class="home">
<div class="row">
    <div class="col-sm-6">
	<a href="student.php"><img src="student.png"height="150px" width="150px"></a>
	<h5>Student</h5>
	</div>
	<div class="col-sm-6">
	<a href="teacher.php"><img src="teacher.png" height="150px" width="150px"></a>
	<h5>Teacher</h5>
    </div>
    <br>
    <br>
	<div class="col-sm-6">
	<a href="course.php"><img src="course.png" height="150px" width="150px"></a>
	<h5>Course</h5>
	</div>
	<div class="col-sm-6">
	<a href="department.php"><img src="department.png" height="150px" width="150px"></a>
	<h5>Department</h5>
	</div>
    
	

</div>
</section>

</body>
</html>